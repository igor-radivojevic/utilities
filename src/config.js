const path = require('path');

const restify = require('restify');

function config(server) {

    server.use(restify.acceptParser(server.acceptable));
    server.use(restify.queryParser());
    server.use(restify.bodyParser({
        maxBodySize: 104857600,
        mapParams: false,
        mapFiles: false,
        keepExtensions: true,
        uploadDir: path.join(__dirname, 'tmp'),
        multiples: false
    }));

    server.use(restify.CORS({
        credentials: true
    }));

    server.use(restify.gzipResponse());

    server.opts(/.*/, function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", req.header("Access-Control-Request-Method"));
        res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
        res.send(200);

        return next();
    });

}

module.exports = config;