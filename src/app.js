const restify = require('restify');

// configuration
const config = require('./config');

// routes
const scrap = require('./routes/scrap');
const compile = require('./routes/compile');

const server = restify.createServer({
    name: 'dragdropr-utils',
    version: '1.0.0'
});

// set all configuration for the server
config(server);

// create endpoints
server.post('/scrap/:resource', scrap);
server.post('/compile/:input/:output', compile);

// listen
server.listen(9874, function () {
    console.log('%s listening at %s', server.name, server.url);
});