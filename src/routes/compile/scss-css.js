const restify = require('restify');
const sass = require('node-sass');

function scssToCss(req, res, next) {
    const { scss } = req.body;

    if (scss) {
        return sass.render({ data: scss }, (error, result) => {
            if (error) {
                return next(new restify.BadRequestError(error.message));
            }

            return res.json({
                data: { css: result.css.toString() }
            });
        });
    } else {
        return next(new restify.BadRequestError(`Field 'scss' is missing`));
    }
}

module.exports = scssToCss;