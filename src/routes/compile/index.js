const restify = require('restify');

const compilers = {
    'scss-css': require('./scss-css')
};

function compile(req, res, next) {
    const { input, output } = req.params;

    const compiler = compilers[`${input}-${output}`];

    if (!compiler) {
        return next(new restify.NotFoundError(`Compile '${input}' to '${output}' not supported`));
    }

    return compiler(req, res, next);
}

module.exports = compile;