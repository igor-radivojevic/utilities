const restify = require('restify');
const getCss = require('get-css');

function css(req, res, next) {
    const { url } = req.body;

    if (url) {
        return getCss(url).then(result => {
            return res.json({
                data: { css: result.css || '' }
            });
        }).catch(function (error) {
            return next(new restify.BadRequestError(error.message));
        });
    } else {
        return next(new restify.BadRequestError(`Field 'url' is missing`));
    }
}

module.exports = css;