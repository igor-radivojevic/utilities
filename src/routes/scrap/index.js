const restify = require('restify');

const scrappers = {
    'css': require('./css')
};

function scrap(req, res, next) {
    const { resource } = req.params;

    const scrapper = scrappers[resource];

    if (!scrapper) {
        return next(new restify.NotFoundError(`Scrap '${resource}' not supported`));
    }

    return scrapper(req, res, next);
}

module.exports = scrap;